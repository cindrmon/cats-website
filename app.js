// import packages -----------------------------------------

    // make sure you have these packages available at packages.json file
    const express = require('express');
    const bodyParser = require('body-parser');
    const ejs = require('ejs');
    const mongoose = require('mongoose');
    require('dotenv-flow').config();

    // insert optional additional packages here

// ---------------------------------------------------------

// express init --------------------------------------------

    const app = express();

    app.set('view engine', 'ejs');

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(express.static("public"));

// ---------------------------------------------------------

// custom global variables ---------------------------------

const Port = process.env.PORT || 3000;

const Config = {
    root_passwd: process.env.ROOT_PASSWD
}

// ---------------------------------------------------------

// mongoose init -------------------------------------------

    mongoose.connect('mongodb+srv://root:' + Config.root_passwd + '@catscollectioncluster.4q3be.gcp.mongodb.net/CatsDB',{useUnifiedTopology:true, useNewUrlParser:true});
    
    // Schemas
    const CAT_NAME_SCHEMA = new mongoose.Schema(
        {
            name: String
        }
    );
    const CAT_TRAIT_SCHEMA = new mongoose.Schema(
        {
            trait: String
        }
    );

    // Models
    CatName = mongoose.model('catname',CAT_NAME_SCHEMA);
    CatTrait = mongoose.model('cattrait',CAT_TRAIT_SCHEMA);

// ---------------------------------------------------------

// insert code here ----------------------------------------

app.get('/', (req,res) => {

    CatName.find((err,foundNames)=>{

        let Random1 = Math.floor(Math.random()*foundNames.length)
        
        let SelectedCatName = foundNames[Random1].name;

        CatTrait.find((err,foundTraits)=>{

            let Random2 = Math.floor(Math.random()*foundTraits.length);

            let SelectedCatTrait = foundTraits[Random2].trait;

            res.render('index',{
                catName: SelectedCatName,
                catTrait: SelectedCatTrait
            })

        });


    });

});
    
// ---------------------------------------------------------

// port listener -------------------------------------------

    app.listen(Port, () => {
        console.log("Server Started on Port " + Port);
        console.log("Press Ctrl+C to Stop Server");
    });

// ---------------------------------------------------------